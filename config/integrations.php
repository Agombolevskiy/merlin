<?php

return [
    /**
     * Конфиг для интеграции с АПИ Merlin AI
     */
    'merlin' => [
        'host' => env('MERLIN_API_HOST')
    ]
];

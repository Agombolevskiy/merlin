<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\UploadController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/form', [UploadController::class, 'form'])->name('form');
Route::get('/', [TaskController::class, 'index'])->name('getTask');
Route::post('/', [UploadController::class, 'upload'])->name('upload');

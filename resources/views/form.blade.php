<style>
    .parent {
        width: 100%;
        height: 100%;
        position: fixed;
        top: 0;
        left: 0;
        display: flex;
        align-items: center;
        align-content: center;
        justify-content: center;
        overflow: auto;
    }

    .row {
        margin: 1em 0;
    }
</style>
<div class="parent">
    <div class="block">
        <form method="post" enctype="multipart/form-data" action="{{ route('upload') }}">
            <div class="row">
                <label for="name">Имя</label>
                <input type="text" name="name" id="name" required>
            </div>
            <div class="row">
                <label for="photo">Фотография</label>
                <input type="file" name="photo" id="photo" required>
            </div>
            <div class="row">
                <input type="submit" value="Отправить">
            </div>
        </form>
    </div>
</div>

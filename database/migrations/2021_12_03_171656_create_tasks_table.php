<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('photo_id');
            $table->boolean('status')->default(false);
            $table->decimal('result', 12, 2)->nullable();
            $table->string('retry_id')->nullable();
            $table->timestamps();

            $table->foreign('photo_id', 'task_to_photo_fk')
                  ->references('id')
                  ->on('photos')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}

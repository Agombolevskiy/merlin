<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;

/**
 * Модель фотографии
 *
 * @property int         $id
 * @property int         $file_id
 * @property string      $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property-read File   $file
 * @property-read Task   $task
 */
class Photo extends Model
{
    /**
     * Файл фотографии
     *
     * @return BelongsTo
     */
    public function file(): BelongsTo
    {
        return $this->belongsTo(File::class);
    }

    /**
     * Задача по фотографии
     *
     * @return HasOne
     */
    public function task(): HasOne
    {
        return $this->hasOne(Task::class);
    }
}

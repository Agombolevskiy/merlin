<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;

/**
 * Модель файла
 *
 * @property int         $id
 * @property int         $user_id
 * @property string      $name
 * @property string      $path
 * @property string      $extension
 * @property string      $mime_type
 * @property int         $size
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property-read Photo  $photo
 */
class File extends Model
{
    /**
     * Фотография, загрузивший файл
     *
     * @return HasOne
     */
    public function photo(): HasOne
    {
        return $this->hasOne(Photo::class);
    }
}

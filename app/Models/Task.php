<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Задачи
 *
 * @property int         $id
 * @property int         $photo_id
 * @property bool        $status
 * @property float|null  $result
 * @property string|null    $retry_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property-read Photo  $photo
 */
class Task extends Model
{
    protected $casts = [
        'status' => 'boolean',
        'result' => 'float'
    ];

    /**
     * Фотография
     *
     * @return BelongsTo
     */
    public function photo(): BelongsTo
    {
        return $this->belongsTo(Photo::class);
    }
}

<?php

namespace App\Services\TaskService\Interfaces;

use App\Models\Task;

/**
 * Интерфейс CRUD сервиса задач
 */
interface TaskServiceInterface
{
    /**
     * Получение модели задачи по ID
     *
     * @param int $taskId
     *
     * @return Task|null
     */
    public function get(int $taskId): ?Task;

    /**
     * Отметить задачу выполненной и сохранить результат
     *
     * @param Task  $task
     * @param float $result
     *
     * @return Task
     */
    public function markAsSuccess(Task $task, float $result): Task;

    /**
     * Отметить задачу ожидающей повтор и сохранить id для повторного запроса
     *
     * @param Task   $task
     * @param string $retryId
     *
     * @return Task
     */
    public function markAsRetry(Task $task, string $retryId): Task;
}

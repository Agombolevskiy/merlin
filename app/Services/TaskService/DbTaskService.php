<?php

namespace App\Services\TaskService;

use App\Jobs\SendPhotoToCheckJob;
use App\Models\Photo;
use App\Models\Task;
use App\Services\TaskService\Interfaces\TaskServiceInterface;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Сервис хранения задач для обработки фотографий
 */
class DbTaskService implements TaskServiceInterface
{
    /**
     * Создание задачи для фото
     *
     * @param Photo $photo
     *
     * @return Task
     * @throws Throwable
     */
    public function create(Photo $photo): Task
    {
        try {
            DB::beginTransaction();

            $task = new Task();
            $task->photo()->associate($photo);
            $task->save();

            dispatch(new SendPhotoToCheckJob($photo));

            DB::commit();
            return $task;
        } catch (Throwable $throwable) {
            DB::rollBack();

            throw $throwable;
        }
    }

    /**
     * @inheritDoc
     *
     * @param int $taskId
     *
     * @return Task|null
     */
    public function get(int $taskId): ?Task
    {
        return Task::find($taskId);
    }

    /**
     * @inheritDoc
     *
     * @param Task  $task
     * @param float $result
     *
     * @return Task
     * @throws Throwable
     */
    public function markAsSuccess(Task $task, float $result): Task
    {
        try {
            DB::beginTransaction();

            $task->status = true;
            $task->result = $result;
            $task->save();

            DB::commit();

            return $task;
        } catch (Throwable $throwable) {
            DB::rollBack();

            throw $throwable;
        }
    }

    /**
     * @inheritDoc
     *
     * @param Task   $task
     * @param string $retryId
     *
     * @return Task
     * @throws Throwable
     */
    public function markAsRetry(Task $task, string $retryId): Task
    {
        try {
            DB::beginTransaction();

            $task->retry_id = $retryId;
            $task->save();
            dispatch(new SendPhotoToCheckJob($task->photo))->delay(10);

            DB::commit();

            return $task;
        } catch (Throwable $throwable) {
            DB::rollBack();

            throw $throwable;
        }
    }
}

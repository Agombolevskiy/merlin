<?php

namespace App\Services\FileService;

use App\Models\File;
use App\Services\FileService\Interfaces\FileServiceInterface;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Http\File as IlluminateFile;
use Throwable;

/**
 * Файловый сервис с локальным хранилищем
 */
class LocalStorageFileService implements FileServiceInterface
{
    /** @var string Директория для загрузки файлов */
    private const UPLOAD_DIRECTORY = 'tmp';

    /** @var Filesystem */
    private Filesystem $storage;

    /**
     * FileService constructor.
     *
     * @param Filesystem $storage
     */
    public function __construct(Filesystem $storage)
    {
        $this->storage = $storage;
    }

    /**
     * Сравнение двух файлов
     *
     * @param string $first
     * @param string $second
     *
     * @return bool
     */
    public function compareFiles(string $first, string $second): bool
    {
        return md5_file($first) === md5_file($second);
    }

    /**
     * @inheritDoc
     *
     * @param UploadedFile $uploadedFile
     *
     * @return File|null
     */
    public function searchInStorage(UploadedFile $uploadedFile): ?File
    {
        foreach (File::all() as $fileModel) {
            if ($this->compareFiles($this->storage->path($fileModel->path), $uploadedFile->getRealPath())) {
                return $fileModel;
            }
        }

        return null;
    }

    /**
     * @inheritDoc
     *
     * @param File $file
     *
     * @return IlluminateFile
     */
    public function getFileFromModel(File $file): IlluminateFile
    {
        return new IlluminateFile($this->storage->path($file->path));
    }

    /**
     * @inheritDoc
     *
     * @param string $path
     *
     * @return File|null
     */
    public function getModelByFilePath(string $path): ?File
    {
        return File::where('path', $path)->first();
    }

    /**
     * @inheritDoc
     *
     * @param UploadedFile $file
     *
     * @return File
     * @throws Throwable
     */
    public function upload(UploadedFile $file): File
    {
        $path = $this->storage->putFile(self::UPLOAD_DIRECTORY, $file);

        try {
            return $this->createModel($file, $path);
        } catch (Throwable $throwable) {
            $this->storage->delete($path);

            throw $throwable;
        }
    }

    /**
     * Создание модели файла
     *
     * @param UploadedFile $file
     * @param string       $path
     *
     * @return File
     * @throws Throwable
     */
    protected function createModel(UploadedFile $file, string $path): File
    {
        try {
            DB::beginTransaction();

            $fileModel = new File();
            $fileModel->name = $file->getClientOriginalName();
            $fileModel->path = $path;
            $fileModel->extension = $file->getClientOriginalExtension();
            $fileModel->mime_type = $file->getClientMimeType();
            $fileModel->size = $file->getSize();
            $fileModel->save();

            DB::commit();

            return $fileModel;
        } catch (Throwable $throwable) {
            DB::rollBack();

            throw $throwable;
        }
    }
}

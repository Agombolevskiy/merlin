<?php

namespace App\Services\FileService\Interfaces;

use App\Models\File as FileModel;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\File;

/**
 * Интерфейс файлового сервиса
 */
interface FileServiceInterface
{
    /**
     * Загрузка файла и создание модели
     *
     * @param UploadedFile $file
     *
     * @return FileModel
     */
    public function upload(UploadedFile $file): FileModel;

    /**
     * Поиск ранее загруженного файла
     *
     * @param UploadedFile $uploadedFile
     *
     * @return File|null
     */
    public function searchInStorage(UploadedFile $uploadedFile): ?FileModel;

    /**
     * Получение файла из модели
     *
     * @param FileModel $file
     *
     * @return File
     */
    public function getFileFromModel(FileModel $file): File;

    /**
     * Поиск модели файла по пути
     *
     * @param string $path
     *
     * @return File|null
     */
    public function getModelByFilePath(string $path): ?FileModel;
}

<?php

namespace App\Services\PhotoService;

use App\Models\File;
use App\Models\Photo;
use App\Services\FileService\Interfaces\FileServiceInterface;
use App\Services\PhotoService\Dto\CreatePhotoDto;
use App\Services\TaskService\Interfaces\TaskServiceInterface;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * CRUD сервис фотографий
 */
class CrudPhotoService
{
    /** @var FileServiceInterface */
    private FileServiceInterface $fileService;

    /** @var TaskServiceInterface */
    private TaskServiceInterface $taskService;

    /**
     * CrudPhotoService constructor.
     *
     * @param FileServiceInterface $fileService
     * @param TaskServiceInterface $taskService
     */
    public function __construct(FileServiceInterface $fileService, TaskServiceInterface $taskService)
    {
        $this->fileService = $fileService;
        $this->taskService = $taskService;
    }

    /**
     * Загрузка файла и создание модели
     *
     * @param CreatePhotoDto $dto
     *
     * @return Photo
     * @throws Throwable
     */
    public function create(CreatePhotoDto $dto): Photo
    {
        $file = $this->fileService->upload($dto->getFile());
        $photo = $this->createModel($file, $dto->getName());
        $this->taskService->create($photo);

        return $photo;
    }

    /**
     * Поиск ранее загруженного файла
     *
     * @param UploadedFile $file
     *
     * @return Photo|null
     */
    public function searchPhoto(UploadedFile $file): ?Photo
    {
        $fileModel = $this->fileService->searchInStorage($file);

        return $fileModel->photo ?? null;
    }

    /**
     * Поиск фотографии по id
     *
     * @param int $id
     *
     * @return Photo|null
     */
    public function get(int $id): ?Photo
    {
        return Photo::find($id);
    }

    /**
     * Создание модели фотографии
     *
     * @param File   $file
     * @param string $name
     *
     * @return Photo
     * @throws Throwable
     */
    protected function createModel(File $file, string $name): Photo
    {
        try {
            DB::beginTransaction();

            $photo = new Photo();
            $photo->name = $name;
            $photo->file()->associate($file);
            $photo->save();

            DB::commit();

            return $photo;
        } catch (Throwable $throwable) {
            DB::rollBack();

            throw $throwable;
        }
    }
}

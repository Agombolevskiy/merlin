<?php

namespace App\Services\PhotoService\Dto;

use Illuminate\Http\UploadedFile;

/**
 * Dto для создания фото
 */
class CreatePhotoDto
{
    private string $name;
    /**
     * @var UploadedFile
     */
    private UploadedFile $file;

    /**
     * CreatePhotoDto constructor.
     *
     * @param string       $name
     * @param UploadedFile $file
     */
    public function __construct(string $name, UploadedFile $file)
    {
        $this->name = $name;
        $this->file = $file;
    }

    /**
     * Получение загруженного файла
     *
     * @return UploadedFile
     */
    public function getFile(): UploadedFile
    {
        return $this->file;
    }

    /**
     * Получение имени
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}

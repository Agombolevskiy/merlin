<?php

namespace App\Services\Integrations\MerlinAi;

use App\Models\Photo;
use App\Services\Integrations\MerlinAi\Client\Dto\CheckPhotoRequestDto;
use App\Services\Integrations\MerlinAi\Client\MerlinAiApi;
use App\Services\Integrations\MerlinAi\Client\Responses\ResponseInterface;
use App\Services\Integrations\MerlinAi\Client\Responses\SuccessResponse;
use App\Services\Integrations\MerlinAi\Client\Responses\WaitResponse;
use App\Services\TaskService\Interfaces\TaskServiceInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Сервис для интеграции с АПИ Merlin AI
 */
class MerlinAiIntegration
{
    /** @var MerlinAiApi */
    private MerlinAiApi $apiClient;
    /** @var TaskServiceInterface */
    private TaskServiceInterface $taskService;

    /**
     * MerlinAiIntegration constructor.
     *
     * @param MerlinAiApi          $apiClient
     * @param TaskServiceInterface $taskService
     */
    public function __construct(
        MerlinAiApi $apiClient,
        TaskServiceInterface $taskService
    ) {
        $this->apiClient = $apiClient;
        $this->taskService = $taskService;
    }

    /**
     * @param Photo $photo
     *
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function checkPhoto(Photo $photo): ResponseInterface
    {
        $response = $this->apiClient->checkPhoto(CheckPhotoRequestDto::buildByPhoto($photo));
        $this->handleCheckPhotoResponse($photo, $response);

        return $response;
    }

    /**
     * Обработка ответа
     *
     * @param Photo             $photo
     * @param ResponseInterface $response
     *
     * @return void
     */
    private function handleCheckPhotoResponse(Photo $photo, ResponseInterface $response): void
    {
        if ($response instanceof SuccessResponse) {
            $this->taskService->markAsSuccess($photo->task, $response->result);
        }

        if ($response instanceof WaitResponse) {
            $this->taskService->markAsRetry($photo->task, $response->retryId);
        }
    }
}

<?php

namespace App\Services\Integrations\MerlinAi\Client;

use App\Services\Integrations\MerlinAi\Client\Dto\CheckPhotoRequestDto;
use App\Services\Integrations\MerlinAi\Client\Dto\MultipartTransformer;
use App\Services\Integrations\MerlinAi\Client\Responses\ResponseFactory;
use App\Services\Integrations\MerlinAi\Client\Responses\ResponseInterface;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Клиент для АПИ Merlin AI
 */
class MerlinAiApi
{
    /** @var Client */
    private Client $httpClient;

    /**
     * MerlinAiApi constructor.
     *
     * @param Client $httpClient
     */
    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * Запрос на проверку фотографии
     *
     * @param CheckPhotoRequestDto $dto
     *
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function checkPhoto(CheckPhotoRequestDto $dto): ResponseInterface
    {
        $response = $this->httpClient->post($this->getUrl(), [
            RequestOptions::MULTIPART => MultipartTransformer::transform($dto)
        ]);
        $response = json_decode($response->getBody()->getContents());

        return ResponseFactory::build($response);
    }

    /**
     * Формирование url запроса
     *
     * @param string $path
     *
     * @return string
     */
    protected function getUrl(string $path = ''): string
    {
        return config('integrations.merlin.host') . '/api/' . $path;
    }
}

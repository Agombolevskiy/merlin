<?php

namespace App\Services\Integrations\MerlinAi\Client\Dto;

use App\Models\Photo;
use App\Services\FileService\LocalStorageFileService;
use Illuminate\Http\File;
use Illuminate\Support\Facades\App;

/**
 * Dto для отправки запроса в АПИ Merlin AI
 */
class CheckPhotoRequestDto
{
    /** @var string */
    protected string $name;
    /** @var File */
    protected File $photo;
    /** @var string|null  */
    protected ?string $retryId;

    /**
     * Сборка объекта из модели
     *
     * @param Photo $photo
     *
     * @return CheckPhotoRequestDto
     */
    public static function buildByPhoto(Photo $photo): CheckPhotoRequestDto
    {
        $fileService = App::make(LocalStorageFileService::class);
        $dto = new static();
        $dto->setName($photo->name);
        $dto->setPhoto($fileService->getFileFromModel($photo->file));
        $dto->setRetryId($photo->task->retry_id);

        return $dto;
    }

    /**
     * Получение файла
     *
     * @return File
     */
    public function getPhoto(): File
    {
        return $this->photo;
    }

    /**
     * Получение имени
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Получение идентификатора для повторного запроса
     *
     * @return string
     */
    public function getRetryId(): ?string
    {
        return $this->retryId;
    }

    /**
     * Сеттер идентификатора для повторного запроса
     *
     * @param string $retryId
     */
    public function setRetryId(?string $retryId): void
    {
        $this->retryId = $retryId;
    }

    /**
     * Сеттер файла
     *
     * @param File $photo
     */
    public function setPhoto(File $photo): void
    {
        $this->photo = $photo;
    }

    /**
     * Сеттер имени
     *
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
}

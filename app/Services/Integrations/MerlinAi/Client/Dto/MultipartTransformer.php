<?php

namespace App\Services\Integrations\MerlinAi\Client\Dto;

use GuzzleHttp\Psr7\Utils;

/**
 * Трансформер Dto объекта в multipart формат
 */
class MultipartTransformer
{
    /**
     * Преобразование Dto объекта в multipart формат
     *
     * @param CheckPhotoRequestDto $dto
     *
     * @return array|array[]
     */
    public static function transform(CheckPhotoRequestDto $dto): array
    {
        $data = [
            [
                'name'     => 'name',
                'contents' => $dto->getName(),
            ],
            [
                'name'     => 'photo',
                'contents' => Utils::tryFopen($dto->getPhoto()->getRealPath(), 'r'),
            ],
        ];
        if ($retryId = $dto->getRetryId()) {
            $data[] = [
                'name'     => 'retry_id',
                'contents' => $retryId,
            ];
        }

        return $data;
    }
}

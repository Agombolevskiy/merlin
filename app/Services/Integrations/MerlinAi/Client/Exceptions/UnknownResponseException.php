<?php

namespace App\Services\Integrations\MerlinAi\Client\Exceptions;

use RuntimeException;

/**
 * Исключение, выбрасываемое при неизвестном ответе АПИ
 */
class UnknownResponseException extends RuntimeException
{

}

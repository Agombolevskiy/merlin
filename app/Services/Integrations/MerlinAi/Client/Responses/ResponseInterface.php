<?php

namespace App\Services\Integrations\MerlinAi\Client\Responses;

/**
 * Интерфейс ответа
 */
interface ResponseInterface
{
    /**
     * Ответ содержит ошибку
     *
     * @return bool
     */
    public function isError(): bool;

    /**
     * Ответ является успешным завершением обработки
     *
     * @return bool
     */
    public function isSuccess(): bool;
}

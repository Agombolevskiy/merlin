<?php

namespace App\Services\Integrations\MerlinAi\Client\Responses;

/**
 * Успешный ответ от АПИ
 */
class SuccessResponse implements ResponseInterface
{
    public string $status;
    public float $result;

    /**
     * SuccessResponse constructor.
     *
     * @param float $result
     */
    public function __construct(float $result)
    {
        $this->status = 'success';
        $this->result = $result;
    }

    /**
     * @inheritDoc
     *
     * @return bool
     */
    public function isError(): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     *
     * @return bool
     */
    public function isSuccess(): bool
    {
        return true;
    }
}

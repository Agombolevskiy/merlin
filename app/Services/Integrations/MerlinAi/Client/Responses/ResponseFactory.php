<?php

namespace App\Services\Integrations\MerlinAi\Client\Responses;

use App\Services\Integrations\MerlinAi\Client\Exceptions\UnknownResponseException;
use stdClass;

/**
 * Фабрика для определения и создания объекта ответа
 */
class ResponseFactory
{
    /**
     * Формирование класса ответа
     *
     * @param stdClass $response
     *
     * @return ResponseInterface
     */
    public static function build(stdClass $response): ResponseInterface
    {
        switch ($response->status ?? null) {
            case 'success';
                return new SuccessResponse($response->result);
                break;
            case 'wait';
                return new WaitResponse($response->retry_id);
                break;
            default:
                throw new UnknownResponseException('Unknown Merlin API response.');
        }
    }
}

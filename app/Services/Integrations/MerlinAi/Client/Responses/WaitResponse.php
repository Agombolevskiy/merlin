<?php

namespace App\Services\Integrations\MerlinAi\Client\Responses;

/**
 * Ответ "Требуется время, запросите результат через пару секунд"
 */
class WaitResponse implements ResponseInterface
{
    public string $status;
    public ?float $result;
    public string $retryId;

    /**
     * SuccessResponse constructor.
     *
     * @param string $retryId
     */
    public function __construct(string $retryId)
    {
        $this->status = 'success';
        $this->result = null;
        $this->retryId = $retryId;
    }

    /**
     * @inheritDoc
     *
     * @return bool
     */
    public function isError(): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     *
     * @return bool
     */
    public function isSuccess(): bool
    {
        return false;
    }
}

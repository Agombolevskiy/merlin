<?php

namespace App\Jobs;

use App\Models\Photo;
use App\Services\Integrations\MerlinAi\MerlinAiIntegration;
use App\Services\PhotoService\CrudPhotoService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Очередь на обработку фотографий
 */
class SendPhotoToCheckJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var CrudPhotoService */
    private CrudPhotoService $photoService;
    /** @var Photo */
    private Photo $photo;

    /**
     * Create job.
     *
     * @param Photo               $photo
     */
    public function __construct(Photo $photo)
    {
        $this->photo = $photo;
    }

    /**
     * Execute the job.
     *
     * @param MerlinAiIntegration $integration
     *
     * @return void
     * @throws GuzzleException
     */
    public function handle(MerlinAiIntegration $integration)
    {
        $this->photo = $this->photo->fresh();
        $this->photo->load(['file', 'task']);

        $integration->checkPhoto($this->photo);
    }
}

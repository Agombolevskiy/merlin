<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetTaskRequest;
use App\Http\Resources\Task;
use App\Services\TaskService\Interfaces\TaskServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

/**
 * Контроллер задач
 */
class TaskController extends Controller
{
    /**
     * Получение задачи по ID
     *
     * @param GetTaskRequest       $request
     * @param TaskServiceInterface $taskService
     *
     * @return JsonResponse
     */
    public function index(GetTaskRequest $request, TaskServiceInterface $taskService): JsonResponse
    {
        if (!$task = $taskService->get($request->input('task_id'))) {
            return response()->json([
                'status' => 'not_found',
                'result' => null
            ], Response::HTTP_NOT_FOUND);
        }

        if (!$task->status) {
            return response()->json((new Task($task))->additional([
                'status' => 'wait'
            ]), Response::HTTP_ACCEPTED);
        }

        return response()->json((new Task($task))->additional([
            'status' => 'ready'
        ]), Response::HTTP_OK);
    }
}

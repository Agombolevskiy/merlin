<?php

namespace App\Http\Controllers;

use App\Http\Requests\UploadPhotoRequest;
use App\Http\Resources\Photo;
use App\Http\Resources\Task;
use App\Services\PhotoService\CrudPhotoService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Throwable;

/**
 * Контроллер загрузки
 */
class UploadController extends Controller
{
    /**
     * Отображение формы создания(для удобства проверки)
     *
     * @return Response
     */
    public function form(): Response
    {
        return response()->view('form');
    }

    /**
     * Метод загрузки\создания фотографии
     *
     * @param UploadPhotoRequest $request
     * @param CrudPhotoService   $photoService
     *
     * @return JsonResponse
     * @throws Throwable
     */
    public function upload(UploadPhotoRequest $request, CrudPhotoService $photoService): JsonResponse
    {
        $dto = $request->getDto();
        if ($photo = $photoService->searchPhoto($dto->getFile())) {
            if (!$photo->task->status) {
                return response()->json((new Task($photo->task))->additional([
                    'status' => 'wait'
                ]), Response::HTTP_ACCEPTED);
            }

            return response()->json((new Task($photo->task))->additional([
                'status' => 'ready'
            ]), Response::HTTP_OK);
        }

        $photo = $photoService->create($dto);

        return response()->json(new Photo($photo), Response::HTTP_CREATED);
    }
}

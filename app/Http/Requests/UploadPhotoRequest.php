<?php

namespace App\Http\Requests;

use App\Services\PhotoService\Dto\CreatePhotoDto;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Валидация добавления фотографии
 */
class UploadPhotoRequest  extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => ['required', 'string'],
            'photo' => ['required', 'image'],
        ];
    }

    /**
     * Формирование Dto из реквеста
     *
     * @return CreatePhotoDto
     */
    public function getDto(): CreatePhotoDto
    {
        return new CreatePhotoDto($this->input('name'), $this->file('photo'));
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Contracts\Support\Arrayable;

/**
 * Ресурс, описывающий фотографию
 */
class Photo extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array|Arrayable
     */
    public function toArray($request)
    {
        return [
            'status' => 'received',
            'task'   => $this->task->id,
            'result' => $this->task->result,
        ];
    }
}

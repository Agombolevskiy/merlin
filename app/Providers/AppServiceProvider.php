<?php

namespace App\Providers;

use App\Services\FileService\Interfaces\FileServiceInterface;
use App\Services\FileService\LocalStorageFileService;
use App\Services\TaskService\DbTaskService;
use App\Services\TaskService\Interfaces\TaskServiceInterface;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        JsonResource::withoutWrapping();

        $this->app->bind(FileServiceInterface::class, LocalStorageFileService::class);
        $this->app->bind(TaskServiceInterface::class, DbTaskService::class);
    }
}
